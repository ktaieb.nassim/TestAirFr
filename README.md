## TestAirFr

Test project

A Springboot API that exposes two services:
• one that allows to register a user
• one that displays the details of a registered user
A user is defined by:
• a user name
• a birthdate
• a country of residence
A user has optional attributes:
• a phone number
• a gender

Only adult French residents are allowed to create an account!
Inputs must be validated and return proper error messages/http statuses.


## Environments :

Project is created with:

* IntelliJ IDEA
* Java 17

## User Stories :

* US 1: Api save

• one that allows to register a user

* US 2: Api load

• one that displays the details of a registered user
