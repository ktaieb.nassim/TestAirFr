package com.fr.airfr.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "user_table")
public class User implements Serializable {

    @Id
    @Column
    @JsonFormat
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    @NotBlank(message = "Name is required")
    @JsonFormat
    private String name;

    @Column
    @NotNull(message = "Birth date is required")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate birthDate;

    @Column
    @JsonFormat
    @Pattern(regexp = "France", message = "only France residence is accepted")
    @NotBlank(message = "Country is required")
    private String country;

    @Column
    @JsonFormat
    private String phone;

    @Column
    @JsonFormat
    private String gender;

    public LocalDate getBirthDate() {
        return birthDate;
    }
}
