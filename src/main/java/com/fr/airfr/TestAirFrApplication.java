package com.fr.airfr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class TestAirFrApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestAirFrApplication.class, args);
    }

}
