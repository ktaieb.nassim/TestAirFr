package com.fr.airfr.services;

import com.fr.airfr.model.User;
import com.fr.airfr.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServices {

    private final UserRepository userRepository;

    UserServices(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser(final User user) {
        return userRepository.save(user);
    }

    public Optional<User> loadUser(final Long id) {
        return userRepository.findById(id);
    }
}
