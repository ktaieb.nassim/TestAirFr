package com.fr.airfr.controller;

import com.fr.airfr.model.User;
import com.fr.airfr.services.UserServices;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("rest")
public class UserController {

    private final UserServices userServices;

    UserController(final UserServices userServices) {
        this.userServices = userServices;
    }

    @PostMapping("/save-user")
    public ResponseEntity saveUser(@Valid @RequestBody final User user, final BindingResult result) {
        if (result.hasErrors()) {
            List<FieldError> allErrors = result.getFieldErrors();
            String errors = allErrors.stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body("The provided user is not valid, " + errors);
        }

        long age = LocalDate.from(user.getBirthDate()).until(LocalDate.now(), ChronoUnit.YEARS);
        if (age < 18) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body("The provided user is not valid, user must be an adult");

        }
        return ResponseEntity.ok(userServices.saveUser(user));
    }

    @GetMapping("/load-user/{id}")
    public ResponseEntity loadUser(@PathVariable("id") Long id) {
        Optional<User> optionalUser = userServices.loadUser(id);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok().body(optionalUser.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User with id '" + id + "'is not found !");
    }
}
